If you're not here because of CS5439 (2023/24), go away.

This is based off of the `LDAPRRefServer` class from this
repository: https://github.com/mbechler/marshalsec/blob/master/src/main/java/marshalsec/jndi/LDAPRefServer.java

## Usage Instructions

1. Open this project in IntelliJ.
2. Build the `CustomMarshalsec.jar` artifact.
3. Refer to the instructions in https://gitlab.com/xantho09-cs5439/attacker-ldap-server
